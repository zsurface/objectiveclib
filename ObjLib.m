#import "ObjLib.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@implementation ObjLib
+(void)print:(CGPoint)p{
    NSLog(@"[%f, %f]", p.x, p.y);
}
+(void)print:(CGPoint)p str:(NSString*)str{
    NSLog(@"%@=[%f, %f]", str, p.x, p.y);
}
+(void)printRect:(CGRect)r str:(NSString*)str{
    NSLog(@"%@=[%f, %f]", str, r.size.width, r.size.height);
}

+(UIBezierPath*)createHexagon:(CGPoint)center radius:(CGFloat)radius{
    UIBezierPath* path = [UIBezierPath bezierPath];
    [path setLineJoinStyle:kCGLineJoinMiter];
    NSInteger num = 6;
    CGFloat interval = 2*M_PI/num;
    
    CGFloat initX = radius*cosf(0*interval);
    CGFloat initY = radius*sinf(0*interval);
    
    [path moveToPoint:CGPointMake(center.x + initX, center.y + initY)];
    for(int i=1; i<num; i++){
        CGFloat x = radius*cosf(i*interval);
        CGFloat y = radius*sinf(i*interval);
        [path addLineToPoint:CGPointMake(center.x + x, center.y + y)];
    }
    [path closePath];
    
    return path;
}

+(UIBezierPath*)createCircle:(CGPoint) center{
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path addArcWithCenter:center radius:50 startAngle:0 endAngle:2 * M_PI clockwise:YES];
    return path;
}

+(CAShapeLayer*)drawHexagon:(CGPoint)center{
    CAShapeLayer* shapeLayer = [CAShapeLayer layer];
    UIBezierPath* path = [self createHexagon:center radius:20.0];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor yellowColor] CGColor];
    shapeLayer.fillColor = [[UIColor brownColor] CGColor];
    shapeLayer.lineWidth = 4.0f;
    return shapeLayer;
}
+(void)Button:(UIView*)view targe:(id)targe selector:(SEL)selector{
    CGSize size = [UIScreen mainScreen].bounds.size;
    UIButton* mybut = [UIButton buttonWithType:UIButtonTypeSystem];
    int height = 40;
    int width = 200;
    CGRect frame = CGRectMake(size.width/2-width/2, 40, width, height);
    mybut.frame = frame;
    [mybut addTarget:targe action:selector forControlEvents:UIControlEventTouchUpInside];
    [mybut setTitle:@"Start Game" forState:(UIControlState) UIControlStateNormal];
    [mybut setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    [mybut.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [mybut setBackgroundColor:[UIColor brownColor]];
    [view addSubview:mybut];
}

+(void)Button:(CGRect)frame  text:(NSString*)text view:(UIView*)view targe:(id)targe selector:(SEL)selector{
    UIButton* mybut = [UIButton buttonWithType:UIButtonTypeSystem];
    mybut.frame = frame;
    [mybut addTarget:targe action:selector forControlEvents:UIControlEventTouchUpInside];
    [mybut setTitle:text forState:(UIControlState) UIControlStateNormal];
    [mybut setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    [mybut.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [mybut setBackgroundColor:[UIColor brownColor]];
    [view addSubview:mybut];
}

+(CAShapeLayer*)CartesianCoordinate{
    CGSize size          = [UIScreen mainScreen].bounds.size;
    
    CAShapeLayer* shapeLayer = [CAShapeLayer layer];
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    // Vertical line
    [path moveToPoint:CGPointMake(size.width/2, 0)];
    [path addLineToPoint:CGPointMake(size.width/2, size.height)];
    
    // Horizontal line
    [path moveToPoint:CGPointMake(0, size.height/2)];
    [path addLineToPoint:CGPointMake(size.width, size.height/2)];
    
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor blackColor] CGColor];
    shapeLayer.fillColor = [[UIColor brownColor] CGColor];
    shapeLayer.lineWidth = 1.0f;
    return shapeLayer;
}

+(CAShapeLayer*)GridLayer:(NSInteger)widthNum height:(NSInteger)heightNum{
    CGSize size          = [UIScreen mainScreen].bounds.size;
    
    CAShapeLayer* shapeLayer = [CAShapeLayer layer];
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    // Vertical line
    CGFloat width = size.width/widthNum;
    CGFloat height = size.height/heightNum;
    for(int i=0; i<widthNum; i++){
        [path moveToPoint:CGPointMake(i*width, 0)];
        [path addLineToPoint:CGPointMake(i*width, size.height)];
    }
    for(int i=0; i<heightNum; i++){
        [path moveToPoint:CGPointMake(0, i*height)];
        [path addLineToPoint:CGPointMake(size.width, i*height)];
    }
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor blackColor] CGColor];
    shapeLayer.fillColor = [[UIColor brownColor] CGColor];
    shapeLayer.lineWidth = 1.0f;
    return shapeLayer;
}

+(CAShapeLayer*)GridLayer:(NSInteger)blockSize width:(NSInteger)widthNum height:(NSInteger)heightNum{
    CGSize size          = [UIScreen mainScreen].bounds.size;
    
    CAShapeLayer* shapeLayer = [CAShapeLayer layer];
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    // Vertical line
    for(int i=0; i<widthNum; i++){
        [path moveToPoint:CGPointMake(i*blockSize, 0)];
        [path addLineToPoint:CGPointMake(i*blockSize, size.height)];
    }
    for(int i=0; i<heightNum; i++){
        [path moveToPoint:CGPointMake(0, i*blockSize)];
        [path addLineToPoint:CGPointMake(size.width, i*blockSize)];
    }
    
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor blackColor] CGColor];
    shapeLayer.fillColor = [[UIColor brownColor] CGColor];
    shapeLayer.lineWidth = 1.0f;
    return shapeLayer;
}

+(void)print1DArray:(int*)array width:(NSInteger)width{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"-------------------------------------");
    for(int i=0; i<width; i++){
        NSLog(@"[%d]", array[i]);
    }
    printf("\n");
    NSLog(@"-------------------------------------");
}

//+(void)print2DNSArray:(NSMutableArray*)array2d width:(NSInteger)width height:(NSInteger)height{
//    NSLog(@"%s", __PRETTY_FUNCTION__);
//    NSLog(@"-------------------------------------");
//    for(int i=0; i<height; i++){
//        for(int j=0; j<width; j++){
//            NSLog(@"[%ld]",(long)[array2d[i][j] integerValue]);
//        }
//        printf("\n");
//    }
//    NSLog(@"-------------------------------------");
//}


@end


