//
//  MyLib.h
//  EmptyApp
//
//  Created by cat on 9/4/15.
//  Copyright (c) 2015 myxcode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include <CoreGraphics/CGBase.h>
#include <CoreGraphics/CGGeometry.h>
#include <CoreGraphics/CGAffineTransform.h>

@interface ObjLib : NSObject

+(void)print:(CGPoint)p;
+(void)print:(CGPoint)p str:(NSString*)str;
+(void)printRect:(CGRect)r str:(NSString*)str;
+(UIBezierPath*)createHexagon:(CGPoint)center radius:(CGFloat)radius;
+(CAShapeLayer*)drawHexagon:(CGPoint)location;
+(UIBezierPath*)createCircle:(CGPoint)center;
+(void)Button:(UIView*)view targe:(id)targe selector:(SEL)selector;
+(void)Button:(CGRect)frame  text:(NSString*)text view:(UIView*)view targe:(id)targe selector:(SEL)selector;
+(CAShapeLayer*)CartesianCoordinate;
+(CAShapeLayer*)GridLayer:(NSInteger)widthNum height:(NSInteger)heightNum;
+(CAShapeLayer*)GridLayer:(NSInteger)blockSize width:(NSInteger)widthNum height:(NSInteger)heightNum;
+(void)print1DArray:(int*)array width:(NSInteger)width;
+(void)print2DNSArray:(NSMutableArray*)array2d width:(NSInteger)width height:(NSInteger)height;


@end
